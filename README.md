# Asistente de Seguridad

## Equipo

Alejandra Cuéllar González A01333324
Cinthya Daniela Lugo Novoa A01332942
Diego Islas Ocampo A01332956
Luis Fernando Saavedra Meza A01333410

## Instrucciones

Puede ayudarlo a...
* Verificar si un correo es potencialmente de phishing
* Verificar si una URL es segura
* Verificar si una contraseña es segura (No funcional por limitaciones del Regex que acepta Watson)

Puede preguntar cosas cómo...

* Qué es phishing
* Qué es la dark web
