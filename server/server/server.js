// Uncomment following to enable zipkin tracing, tailor to fit your network configuration:
// const appzip = require('appmetrics-zipkin')({
//     host: 'localhost',
//     port: 9411,
//     serviceName:'frontend'
// });

require('appmetrics-dash').attach();
require('appmetrics-prometheus').attach();
const appName = require('./../package').name;
const http = require('http');
const express = require('express');
const log4js = require('log4js');
const localConfig = require('./config/local.json');
const path = require('path');

const logger = log4js.getLogger(appName);
const app = express();
const server = http.createServer(app);

app.use(log4js.connectLogger(logger, {
  level: process.env.LOG_LEVEL || 'info'
}));
const serviceManager = require('./services/service-manager');
require('./services/index')(app);
require('./routers/index')(app, server);

const bodyParser = require('body-parser');
// is this needed .... yes absolutely needed
// to get constiables from views form to app.js
// if needed , it has to be before app.router
//app.use(express.urlencoded());
app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

// Add your code here
// const prompt = require('prompt-sync')();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

const watson = require('watson-developer-cloud');

const assistant = new watson.AssistantV1({
  iam_apikey: 'vbvzkI1AyHkLJ8Zy7HluySR6lFSajy9RQML3vMYIr0Cs',
  version: '2018-09-20'
});

app.post('/converse', (req, res) => {
  console.log(req.body);

  assistant.message({
    workspace_id: '8bfbeba1-beeb-403b-b941-dfeff84dec1f',
    input: {
      'text': req.body.dialog
    },
    context: req.body.context
  }, (error, response) => error ? console.log(error) : res.json(response));
});

const port = process.env.PORT || localConfig.port;
server.listen(port, function () {
  logger.info(`openthedoorhal listening on http://localhost:${port}/appmetrics-dash`);
  logger.info(`openthedoorhal listening on http://localhost:${port}`);
});

app.use(function (req, res, next) {
  res.sendFile(path.join(__dirname, '../public', '404.html'));
});

app.use(function (err, req, res, next) {
  res.sendFile(path.join(__dirname, '../public', '500.html'));
});
module.exports = server;